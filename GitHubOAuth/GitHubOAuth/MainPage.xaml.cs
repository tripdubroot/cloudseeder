﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GitHubOAuth
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            var user = txtUsername.Text;
            var pass = txtPasword.Password;
            var clientId = "4675546a46c4d45a1de5";
            var clientSecret = "6573899f528275f19c95791bc0c58305c5aa3eee";
            var authAppBoday = new GitHubAuthBodyForApp { client_secret = clientSecret, scopes = new string[] { "repo", "user" } };
            var URL = String.Format("https://api.github.com/authorizations/clients/{0}", clientId);

            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes(String.Format("{0}:{1}", user, pass));
            client.DefaultRequestHeaders.Authorization = new HttpCredentialsHeaderValue("Basic", Convert.ToBase64String(byteArray));
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2;  WOW64; Trident / 6.0)");

            var body = JsonConvert.SerializeObject(authAppBoday);
            var httpStringContent = new HttpStringContent(body, UnicodeEncoding.Utf8, "application/json");

            string result = string.Empty;

            try
            {
                var response = await client.PutAsync(new Uri(URL), httpStringContent);
                result = await response.Content.ReadAsStringAsync();
            }
            catch (Exception up)
            {
                throw up;
            }

            client.Dispose();

            MessageDialog msg = new MessageDialog(result);
            await msg.ShowAsync();
        }
    }
}