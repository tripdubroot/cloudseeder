﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Authentication.Web;
using Windows.Security.Credentials;

namespace deployToAzure.libs.Common
{
    public class Globals
    {
        public static string Token;
        public const string clientId = "d0c930d1-09be-434d-afbc-acd2716e3211";
        public const string authority = "organizations";
        public const string resource = "https://management.core.windows.net/";
        public static readonly string URI = string.Format("ms-appx-web://Microsoft.AAD.BrokerPlugIn/{0}", WebAuthenticationBroker.GetCurrentApplicationCallbackUri().Host.ToUpper());
        public static WebAccount userAccount;
    }
}
