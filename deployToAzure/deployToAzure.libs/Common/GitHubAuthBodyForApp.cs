﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace deployToAzure.libs.Common
{
    class GitHubAuthBodyForApp
    {
        public string client_secret { get; set; }
        public string[] scopes { get; set; }
    }
}
