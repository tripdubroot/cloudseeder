﻿using System;
using System.Threading.Tasks;
using Windows.Web.Http;
using Windows.Web.Http.Headers;
using UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding;

namespace deployToAzure.libs.Controllers
{
    public class WebRequestController
    {
        private WebRequestControllerParameters _parameters;

        public WebRequestController(WebRequestControllerParameters parameters)
        {
            _parameters = parameters;
        }

        public async Task<string> GetWebRequest()
        {
            var uri = new Uri(_parameters.Url);
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("User-Agent",
                "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2;  WOW64; Trident / 6.0)");
            string result = string.Empty;

            try
            {
                var response = await client.GetAsync(uri);
                result = await response.Content.ReadAsStringAsync();
            }
            catch (Exception up)
            {
                throw up;
            }

            client.Dispose();

            return result;
        }

        public async Task<string> GetAuthenticatedWebRequest()
        {
            var uri = new Uri(_parameters.Url);
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("User-Agent",
                "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2;  WOW64; Trident / 6.0)");
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _parameters.Token);
            string result = string.Empty;

            try
            {
                var response = await client.GetAsync(uri);
                result = await response.Content.ReadAsStringAsync();
            }
            catch (Exception up)
            {
                throw up;
            }

            client.Dispose();

            return result;
        }

        public async Task<String> PostWebRequest()
        {
            var uri = new Uri(_parameters.Url);
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("User-Agent",
                "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2;  WOW64; Trident / 6.0)");
            string result = string.Empty;

            client.DefaultRequestHeaders.Accept.Add(new HttpMediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                HttpResponseMessage response = await client.PostAsync(uri,
                    new HttpStringContent(_parameters.PostData, UnicodeEncoding.Utf8, "application/json"));

                result = response.StatusCode.ToString();
            }
            catch (Exception up)
            {
                throw up;
            }

            client.Dispose();

            return result;
        }
    }
}
