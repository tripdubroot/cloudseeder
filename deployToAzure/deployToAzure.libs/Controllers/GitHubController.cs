﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Octokit;
using Windows.Web.Http;
using UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding;
using Windows.Web.Http.Headers;
using System;
using Newtonsoft.Json;
using System.Text;
using deployToAzure.libs.Common;

namespace deployToAzure.libs.Controllers
{
    public class GitHubController
    {
        private readonly GitHubClient _client;
        private GitHubControllerParameters _parameters;
        private string _apiToken = string.Empty;
        private Credentials _credentials;

        public GitHubController(GitHubControllerParameters parameters)
        {
            _parameters = parameters;
            _apiToken = _parameters.APIKey;
            _credentials = new Credentials(_apiToken);

            _client = new GitHubClient(new ProductHeaderValue("deployToAzure"));
            _client.Credentials = _credentials;
        }

        public async Task<IReadOnlyList<Repository>> GetRepositoryList()
        {
            IReadOnlyList<Repository> result;
            try
            {
                result = await _client.Repository.GetAllForUser(_parameters.GitHubUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public async Task<User> GetUserInfo()
        {
            return await _client.User.Get(_parameters.GitHubUser);
        }

        public async Task<Repository> GetRepository()
        {
            return await _client.Repository.Get(_parameters.GitHubUser, _parameters.RepositoryName);
        }

        public static async Task<string> GetGitAPIToken(string gitHubUsername, string gitHubPassword)
        {
            var clientId = "4675546a46c4d45a1de5";
            var clientSecret = "6573899f528275f19c95791bc0c58305c5aa3eee";
            var authAppBoday = new GitHubAuthBodyForApp { client_secret = clientSecret, scopes = new string[] { "repo", "user" } };
            var URL = string.Format("https://api.github.com/authorizations/clients/{0}", clientId);

            HttpClient client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", gitHubUsername, gitHubPassword));
            client.DefaultRequestHeaders.Authorization = new HttpCredentialsHeaderValue("Basic", Convert.ToBase64String(byteArray));
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2;  WOW64; Trident / 6.0)");

            var body = JsonConvert.SerializeObject(authAppBoday);
            var httpStringContent = new HttpStringContent(body, UnicodeEncoding.Utf8, "application/json");

            string result = string.Empty;

            try
            {
                var response = await client.PutAsync(new Uri(URL), httpStringContent);
                result = await response.Content.ReadAsStringAsync();
            }
            catch (Exception up)
            {
                throw up;
            }

            client.Dispose();

            return result;
        }
    }
}
