﻿namespace deployToAzure.libs.Controllers
{
    public class WebRequestControllerParameters
    {
        public string Url { get; set; }
        public string PostData { get; set; }
        public string Token { get; set; }
    }
}
