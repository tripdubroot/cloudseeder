﻿using System;

namespace deployToAzure.libs.Controllers
{
    public class ResourceManagerControllerParameters
    {
        public string SubscriptionId { get; set; }
        public string Token { get; set; }
        public string ResourceGroupName { get; set; }
        public string ResourceGroupLocation { get; set; }
        public string DeploymentName { get; set; }
        public Uri ParameterFileUri { get; set; }
        public Uri TemplateFileUri { get; set; }

    }
}