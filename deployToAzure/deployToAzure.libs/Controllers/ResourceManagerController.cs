﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Azure.Management.Resources;
using Microsoft.Azure.Management.Resources.Models;

namespace deployToAzure.libs.Controllers
{
    public class ResourceManagerController : IDisposable
    {
        private readonly ResourceManagementClient _rgClient;
        private readonly ResourceManagerControllerParameters _parameters;


        public ResourceManagerController(ResourceManagerControllerParameters parameters)
        {
            _parameters = parameters;
            var rgCred = new TokenCloudCredentials(_parameters.SubscriptionId, _parameters.Token);
            _rgClient = new ResourceManagementClient(rgCred);
        }

        public async Task<ResourceGroupExistsResult> CheckIfResourceGroupExists()
        {
            var value = await _rgClient.ResourceGroups.CheckExistenceAsync(_parameters.ResourceGroupName);

            return value;
        }

        public async Task<ResourceGroupCreateOrUpdateResult> CreateResourceGroup()
        {
            var value = await _rgClient.ResourceGroups.CreateOrUpdateAsync(_parameters.ResourceGroupName, new ResourceGroup
            {
                Location = _parameters.ResourceGroupLocation
            });

            return value;
        }

        public async Task<DeploymentOperationsCreateResult> DeployResources()
        {
            var result = await
                _rgClient.Deployments.CreateOrUpdateAsync(_parameters.ResourceGroupName, _parameters.DeploymentName,
                    new Deployment
                    {
                        Properties = new DeploymentProperties
                        {
                            ParametersLink = new ParametersLink(_parameters.ParameterFileUri),
                            TemplateLink = new TemplateLink(_parameters.TemplateFileUri)
                        }
                    });

            return result;
        }

        public void Dispose()
        {
            _rgClient?.Dispose();
        }
    }
}
