﻿namespace deployToAzure.libs.Controllers
{
    public class GitHubControllerParameters
    {
        public string GitHubUser { get; set; }
        public string RepositoryName { get; set; }
        public string ContentUri => string.Format("https://api.github.com/repos/{0}/{1}/contents", GitHubUser, RepositoryName);
        public string JsonUris => string.Format("https://api.github.com/search/code?q=json+in:path+language:json+repo:{0}/{1}", GitHubUser, RepositoryName);
        public string APIKey { get; set; }
    }
}
