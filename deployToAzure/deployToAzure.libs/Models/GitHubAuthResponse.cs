﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace deployToAzure.libs.Models
{

    public class App
    {
        public string name { get; set; }
        public string url { get; set; }
        public string client_id { get; set; }
    }

    public class GitHubAuthResponse
    {
        public int id { get; set; }
        public string url { get; set; }
        public App app { get; set; }
        public string token { get; set; }
        public string hashed_token { get; set; }
        public string token_last_eight { get; set; }
        public object note { get; set; }
        public object note_url { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public List<string> scopes { get; set; }
        public object fingerprint { get; set; }
    }

}
