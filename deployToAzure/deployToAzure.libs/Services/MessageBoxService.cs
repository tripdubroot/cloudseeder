﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace deployToAzure.libs.Services
{
    class MessageBoxService: IMessageBoxService
    {
        public async void ShowMessage(string msg)
        {
            if (msg == null)
                msg = "No message was passed to this pop-up. The programmer made a mistake.";

            MessageDialog dialog = new MessageDialog(msg);
            await dialog.ShowAsync();
        }
    }
}
