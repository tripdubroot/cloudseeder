﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

namespace deployToAzure
{
    public static class AuthHelper
    {
        public async static Task<string> GetAuthorizationHeader()
        {
            var context = new AuthenticationContext("https://login.windows.net/common"); // tenant agnostic endpooint
            var result = await context.AcquireTokenAsync("https://graph.windows.net",
                                          "1950a258-227b-4e31-a9cf-717495945fc2", // Windows Azure Management API's Client ID
                                          new Uri("http://li")); // standard redirect for native apps (console, desktop, mobile, etc) new Uri("urn:ietf:wg:oauth:2.0:oob")

            return result.AccessToken;
        }
    }
}
