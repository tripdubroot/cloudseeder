﻿using System;
using Windows.Security.Authentication.Web;
using Windows.Security.Authentication.Web.Core;
using Windows.Security.Credentials;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using deployToAzure.libs.Controllers;
using deployToAzure.libs.ViewModels;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace deployToAzure.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page //, INotifyPropertyChanged
    {
        ApplicationDataContainer appSettings = null;
        const string clientId = "d0c930d1-09be-434d-afbc-acd2716e3211";
        const string authority = "organizations";
        const string resource = "https://management.core.windows.net/";
        string URI = string.Format("ms-appx-web://Microsoft.AAD.BrokerPlugIn/{0}", WebAuthenticationBroker.GetCurrentApplicationCallbackUri().Host.ToUpper());

        WebAccountProvider wap = null;
        WebAccount userAccount = null;
        string token = string.Empty;
        private MainPageViewModel ViewModel;

        //public event PropertyChangedEventHandler PropertyChanged;

        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;

            ViewModel = new MainPageViewModel();
            this.DataContext = ViewModel;
        }

        //public MainPageViewModel ViewModel => this.DataContext as MainPageViewModel;

        private async void showMessage(string thing)
        {
            if (thing == null)
                thing = "Yo man! There was an issue!";

            //MessageDialog msg = new MessageDialog(thing);
            //await msg.ShowAsync();

            ViewModel.GetMessage();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            wap = await WebAuthenticationCoreManager.FindAccountProviderAsync("https://login.microsoft.com", authority);
            appSettings = ApplicationData.Current.RoamingSettings;
            WebTokenRequest wtr = new WebTokenRequest(wap, string.Empty, clientId);
            wtr.Properties.Add("resource", resource);

            // Check if there's a record of the last account used with the app
            var userID = appSettings.Values["userID"];
            if (userID != null)
            {
                // Get an account object for the user
                userAccount = await WebAuthenticationCoreManager.FindAccountAsync(wap, (string)userID);
                if (userAccount != null)
                {
                    // Ensure that the saved account works for getting the token we need
                    WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr, userAccount);
                    if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
                    {
                        userAccount = wtrr.ResponseData[0].WebAccount;
                        token = wtrr.ResponseData[0].Token;
                    }
                    else
                    {
                        // The saved account could not be used for getitng a token
                        MessageDialog messageDialog = new MessageDialog("We tried to sign you in with the last account you used with this app, but it didn't work out. Please sign in as a different user.");
                        await messageDialog.ShowAsync();
                        // Make sure that the UX is ready for a new sign in
                        UpdateUXonSignOut();
                    }
                }
                else
                {
                    // The WebAccount object is no longer available. Let's attempt a sign in with the saved username
                    wtr.Properties.Add("LoginHint", appSettings.Values["login_hint"].ToString());
                    WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr);
                    if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
                    {
                        userAccount = wtrr.ResponseData[0].WebAccount;
                        token = wtrr.ResponseData[0].Token;
                    }
                }
            }
            else
            {
                // There is no recorded user. Let's start a sign in flow without imposing a specific account.                             
                WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr);
                if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
                {
                    userAccount = wtrr.ResponseData[0].WebAccount;
                    token = wtrr.ResponseData[0].Token;
                }
            }

            if (userAccount != null) // we succeeded in obtaining a valid user
            {
                // save user ID in local storage
                UpdateUXonSignIn();
            }
            else
            {
                // nothing we tried worked. Ensure that the UX reflects that there is no user currently signed in.
                UpdateUXonSignOut();
                MessageDialog messageDialog = new MessageDialog("We could not sign you in. Please try again.");
                await messageDialog.ShowAsync();
            }

            //if (PropertyChanged != null)
            //    PropertyChanged(this, new PropertyChangedEventArgs(nameof(token)));
        }

        // Change the currently signed in user
        private async void btnSignInOut_Click(object sender, RoutedEventArgs e)
        {
            // prepare a request with 'WebTokenRequestPromptType.ForceAuthentication', 
            // which guarantees that the user will be able to enter an account of their choosing
            // regardless of what accounts are already present on the system
            WebTokenRequest wtr = new WebTokenRequest(wap, string.Empty, clientId, WebTokenRequestPromptType.ForceAuthentication);
            wtr.Properties.Add("resource", resource);
            WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr);
            if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
            {
                userAccount = wtrr.ResponseData[0].WebAccount;
                token = wtrr.ResponseData[0].Token;
                UpdateUXonSignIn();
            }
            else
            {
                UpdateUXonSignOut();
                MessageDialog messageDialog = new MessageDialog("We could not sign you in. Please try again.");
                await messageDialog.ShowAsync();
            }
        }

        // update the UX and the app settings to show that a user is signed in
        private void UpdateUXonSignIn()
        {
            appSettings.Values["userID"] = userAccount.Id;
            appSettings.Values["login_hint"] = userAccount.UserName;
            textSignedIn.Text = string.Format("you are signed in as {0} - ", userAccount.UserName);
            btnSignInOut.Content = "Sign in as a different user";
            //btnSearch.IsEnabled = true;
        }

        // update the UX and the app settings to show that no user is signed in at the moment
        private void UpdateUXonSignOut()
        {
            appSettings.Values["userID"] = null;
            appSettings.Values["login_hint"] = null;
            //btnSearch.IsEnabled = false;
            textSignedIn.Text = "You are not signed in. ";
            btnSignInOut.Content = "Sign in";
            //SearchResults.ItemsSource = new List<UserSearchResult>();
        }

        private async void btnSetup_Click(object sender, RoutedEventArgs e)
        {
            ResourceManagerControllerParameters parameters = new ResourceManagerControllerParameters
            {
                DeploymentName = "ipdep01", //txtDeploymentName.Text,
                ParameterFileUri = new Uri("https://raw.githubusercontent.com/devdash/testdeployments/master/WebSite.param.dev.json"), //new Uri(txtParameterFile.Text),
                ResourceGroupLocation = "eastus", //txtLocation.Text,
                ResourceGroupName = "iptestrg", //txtResourceGroup.Text,
                SubscriptionId = "2295f62b-34e7-40a1-9e9f-6def6b9f20b7", //txtSubId.Text.Trim(),
                TemplateFileUri = new Uri("https://raw.githubusercontent.com/devdash/testdeployments/master/WebSite.json"), //new Uri(txtTemplateFile.Text),
                Token = token
            };

            ResourceManagerController controller = new ResourceManagerController(parameters);

            try
            {
                var group = await controller.CheckIfResourceGroupExists();

                if (!group.Exists)
                {
                    var rgResult = await controller.CreateResourceGroup();
                    showMessage("Create resource group result: " + rgResult.StatusCode.ToString());

                    var depResult = await controller.DeployResources();
                    showMessage("Create deployment result: " + depResult.StatusCode);
                }
            }
            catch (Exception up)
            {
                showMessage(up.Message);
            }

        }

        private async void btnGitHub_Click(object sender, RoutedEventArgs e)
        {
            GitHubControllerParameters parameters = new GitHubControllerParameters
            {
                GitHubUser = "devdash",
                RepositoryName = "testdeployments"
            };

            WebRequestControllerParameters webParameters = new WebRequestControllerParameters
            {
                Url = parameters.JsonUris
            };

            GitHubController controller = new GitHubController(parameters);
            WebRequestController webController = new WebRequestController(webParameters);

            var result = await webController.GetWebRequest();

            var user = await controller.GetUserInfo();
            var repo = await controller.GetRepository();



            showMessage(result);
        }
    }
}
