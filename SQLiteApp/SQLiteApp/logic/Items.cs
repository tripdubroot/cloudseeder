﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Attributes;
using System.IO;


namespace SQLiteApp.logic
{
    public class Stock
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(8)]
        public string Symbol { get; set; }
    }

    public class DataAccessor
    {
        private SQLiteAsyncConnection _connection;

        public DataAccessor()
        {
            var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");
            SQLiteConnectionString connString = new SQLiteConnectionString(path, false);
            SQLiteConnectionWithLock conn = new SQLiteConnectionWithLock(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), connString);

            _connection = new SQLiteAsyncConnection(() => conn);
        }

        public async void CreateTable()
        {
            await _connection.CreateTableAsync<Stock>();
        }

        public async void Create(Stock stock)
        {
            await _connection.InsertAsync(stock);
        }

        public async Task<Stock> Get(string stock)
        {
            var query = _connection.Table<Stock>().Where(v => v.Symbol.Contains(stock));
            return query.ToListAsync().Result.First();
        }

        public async void Update(Stock stock)
        {
            await _connection.UpdateAsync(stock);
        }

        public async void Delete(Stock stock)
        {
            await _connection.DeleteAsync(stock);
        }
    }
}
