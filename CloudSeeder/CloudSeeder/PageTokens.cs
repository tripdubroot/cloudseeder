﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudSeeder
{
    public static class PageTokens
    {
        public const string MainPage = "Main";
        public const string SecondPage = "Second";
        public const string AzureLoginPage = "AzureLogin";
        public const string SubscriptionListPage = "SubscriptionList";
        public const string GitHubLoginPage = "GitHubLogin";
        public const string AddManualPage = "AddManual";
        public const string AddGitHubRepoPage = "AddGitHubRepo";
        public const string SettingsPage = "Settings";
        public const string AddTemplateFilePage = "AddTemplateFile";
        public const string AddParameterFilePage = "AddParameterFile";
    }
}
