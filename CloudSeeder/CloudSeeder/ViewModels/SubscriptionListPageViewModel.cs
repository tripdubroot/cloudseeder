﻿using Prism.Commands;
using Prism.Windows.AppModel;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;
using deployToAzure.libs.Common;
using Windows.Storage;
using Windows.Security.Credentials;
using Windows.Security.Authentication.Web;
using Windows.Security.Authentication.Web.Core;
using System.ComponentModel;
using deployToAzure.libs.Controllers;
using deployToAzure.libs.Models;
using Newtonsoft.Json;
using Windows.UI.Xaml.Controls;
using System.Collections.ObjectModel;

namespace CloudSeeder.ViewModels
{
    public class SubscriptionListPageViewModel : ViewModelBase, INotifyPropertyChanged
    {
        public string SignedInText { get; private set; }
        public event PropertyChangedEventHandler PropertyChanged;
        ApplicationDataContainer appSettings = null;
        WebAccountProvider wap = null;
        public DelegateCommand NextPageCommand { get; private set; }
        private readonly INavigationService _navigationService;
        private SubscriptionsRootObject subscriptionList;
        public ObservableCollection<Value> SubList { get; set; }

        public Value SelectedValue { get; set; }

        // Allows me to fire when value changed in UI
        //private Value _selectedValue;

        //public Value SelectedValue
        //{
        //    get { return _selectedValue; }
        //    set {
        //        _selectedValue = value;
        //        int i = 0;
        //    }
        //}

        public SubscriptionListPageViewModel(INavigationService navigationService, IResourceLoader resourceLoader)
        {
            _navigationService = navigationService;
            NextPageCommand = DelegateCommand.FromAsyncHandler(MoveToNextPage, canMoveToNextPage);
            SubList = new ObservableCollection<Value>();
        }

        private async Task MoveToNextPage()
        {
            appSettings.Values["subscriptionName"] = SelectedValue.displayName;
            appSettings.Values["subscriptionId"] = SelectedValue.subscriptionId;
            _navigationService.Navigate(PageTokens.GitHubLoginPage, null);
        }

        public override void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            LoginRequest();
            base.OnNavigatedTo(e, viewModelState);
        }

        public virtual async Task LoginRequest()
        {
            wap = await WebAuthenticationCoreManager.FindAccountProviderAsync("https://login.microsoft.com", Globals.authority);
            appSettings = ApplicationData.Current.RoamingSettings;
            WebTokenRequest wtr = new WebTokenRequest(wap, string.Empty, Globals.clientId);
            wtr.Properties.Add("resource", Globals.resource);

            // Check if there's a record of the last account used with the app
            var userID = appSettings.Values["userID"];
            if (userID != null)
            {
                // Get an account object for the user
                Globals.userAccount = await WebAuthenticationCoreManager.FindAccountAsync(wap, (string)userID);
                if (Globals.userAccount != null)
                {
                    // Ensure that the saved account works for getting the token we need
                    WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr, Globals.userAccount);
                    if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
                    {
                        Globals.userAccount = wtrr.ResponseData[0].WebAccount;
                        Globals.Token = wtrr.ResponseData[0].Token;
                    }
                    else
                    {
                        // The saved account could not be used for getitng a token
                        MessageDialog messageDialog = new MessageDialog("We tried to sign you in with the last account you used with this app, but it didn't work out. Please sign in as a different user.");
                        await messageDialog.ShowAsync();
                    }
                }
                else
                {
                    // The WebAccount object is no longer available. Let's attempt a sign in with the saved username
                    wtr.Properties.Add("LoginHint", appSettings.Values["login_hint"].ToString());
                    WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr);
                    if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
                    {
                        Globals.userAccount = wtrr.ResponseData[0].WebAccount;
                        Globals.Token = wtrr.ResponseData[0].Token;
                    }
                }
            }
            else
            {
                // There is no recorded user. Let's start a sign in flow without imposing a specific account.                             
                WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr);
                if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
                {
                    Globals.userAccount = wtrr.ResponseData[0].WebAccount;
                    Globals.Token = wtrr.ResponseData[0].Token;
                }
            }

            if (Globals.userAccount != null) // we succeeded in obtaining a valid user
            {
                // save user ID in local storage
                UpdateUXonSignIn();
            }

            var webReqParams = new WebRequestControllerParameters { Url = "https://management.azure.com/subscriptions?api-version=2015-01-01", Token = Globals.Token };
            var webRequest = new WebRequestController(webReqParams);

            var response = await webRequest.GetAuthenticatedWebRequest();

            subscriptionList = JsonConvert.DeserializeObject<SubscriptionsRootObject>(response);

            foreach (var item in subscriptionList.value)
                SubList.Add(item);
        }

        private void UpdateUXonSignIn()
        {
            appSettings.Values["userID"] = Globals.userAccount.Id;
            appSettings.Values["login_hint"] = Globals.userAccount.UserName;
            SignedInText = string.Format("Welcome, {0}", Globals.userAccount.UserName);
            //SignInOutContent = "Sign in as a different user";

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(SignedInText)));
                //PropertyChanged(this, new PropertyChangedEventArgs(nameof(SignInOutContent)));
            }

        }

        private bool canMoveToNextPage()
        {
            return true;
        }

        // *** IN CONSTRUCTOR ***
        //LoginCommand = DelegateCommand.FromAsyncHandler(Login, canLogin);
        //SignInSignOutCommand = DelegateCommand.FromAsyncHandler(SignInSignOut, canSignInSignOut);
        // *** IN CONSTRUCTOR ***
        //public DelegateCommand LoginCommand { get; private set; }
        //public DelegateCommand SignInSignOutCommand { get; private set; }
        //public string SignInOutContent { get; private set; }
        //public async Task SignInSignOut()
        //{
        //    // prepare a request with 'WebTokenRequestPromptType.ForceAuthentication', 
        //    // which guarantees that the user will be able to enter an account of their choosing
        //    // regardless of what accounts are already present on the system
        //    WebTokenRequest wtr = new WebTokenRequest(wap, string.Empty, Globals.clientId, WebTokenRequestPromptType.ForceAuthentication);
        //    wtr.Properties.Add("resource", Globals.resource);
        //    WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr);
        //    if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
        //    {
        //        Globals.userAccount = wtrr.ResponseData[0].WebAccount;
        //        Globals.Token = wtrr.ResponseData[0].Token;
        //        UpdateUXonSignIn();
        //    }
        //    else
        //    {
        //        UpdateUXonSignOut();
        //        MessageDialog messageDialog = new MessageDialog("We could not sign you in. Please try again.");
        //        await messageDialog.ShowAsync();
        //    }
        //}

        //public virtual async Task Login()
        //{
        //    wap = await WebAuthenticationCoreManager.FindAccountProviderAsync("https://login.microsoft.com", Globals.authority);
        //    appSettings = ApplicationData.Current.RoamingSettings;
        //    WebTokenRequest wtr = new WebTokenRequest(wap, string.Empty, Globals.clientId);
        //    wtr.Properties.Add("resource", Globals.resource);

        //    // Check if there's a record of the last account used with the app
        //    var userID = appSettings.Values["userID"];
        //    if (userID != null)
        //    {
        //        // Get an account object for the user
        //        Globals.userAccount = await WebAuthenticationCoreManager.FindAccountAsync(wap, (string)userID);
        //        if (Globals.userAccount != null)
        //        {
        //            // Ensure that the saved account works for getting the token we need
        //            WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr, Globals.userAccount);
        //            if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
        //            {
        //                Globals.userAccount = wtrr.ResponseData[0].WebAccount;
        //                Globals.Token = wtrr.ResponseData[0].Token;
        //            }
        //            else
        //            {
        //                // The saved account could not be used for getitng a token
        //                MessageDialog messageDialog = new MessageDialog("We tried to sign you in with the last account you used with this app, but it didn't work out. Please sign in as a different user.");
        //                await messageDialog.ShowAsync();
        //                // Make sure that the UX is ready for a new sign in
        //                UpdateUXonSignOut();
        //            }
        //        }
        //        else
        //        {
        //            // The WebAccount object is no longer available. Let's attempt a sign in with the saved username
        //            wtr.Properties.Add("LoginHint", appSettings.Values["login_hint"].ToString());
        //            WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr);
        //            if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
        //            {
        //                Globals.userAccount = wtrr.ResponseData[0].WebAccount;
        //                Globals.Token = wtrr.ResponseData[0].Token;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        // There is no recorded user. Let's start a sign in flow without imposing a specific account.                             
        //        WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr);
        //        if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
        //        {
        //            Globals.userAccount = wtrr.ResponseData[0].WebAccount;
        //            Globals.Token = wtrr.ResponseData[0].Token;
        //        }
        //    }

        //    if (Globals.userAccount != null) // we succeeded in obtaining a valid user
        //    {
        //        // save user ID in local storage
        //        UpdateUXonSignIn();
        //    }
        //    else
        //    {
        //        // nothing we tried worked. Ensure that the UX reflects that there is no user currently signed in.
        //        UpdateUXonSignOut();
        //        MessageDialog messageDialog = new MessageDialog("We could not sign you in. Please try again.");
        //        await messageDialog.ShowAsync();
        //    }

        //    // _navigationService.Navigate(PageTokens.SubscriptionListPage, null);
        //}

        // update the UX and the app settings to show that a user is signed in



        // update the UX and the app settings to show that no user is signed in at the moment
        //private void UpdateUXonSignOut()
        //{
        //    appSettings.Values["userID"] = null;
        //    appSettings.Values["login_hint"] = null;
        //    Globals.userAccount = null;
        //    Globals.Token = null;
        //    SignedInText = "You are not signed in. ";
        //    SignInOutContent = "Sign in";

        //    if (PropertyChanged != null)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(nameof(SignedInText)));
        //        PropertyChanged(this, new PropertyChangedEventArgs(nameof(SignInOutContent)));
        //    }
        //}

        //private bool canLogin()
        //{
        //    return true;
        //}

        //private bool canSignInSignOut()
        //{
        //    return true;
        //}
    }
}
