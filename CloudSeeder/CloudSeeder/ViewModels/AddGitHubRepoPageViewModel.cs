﻿using Prism.Windows.Mvvm;
using deployToAzure.libs.Controllers;
using Windows.Storage;
using Octokit;
using System.Collections.Generic;
using Prism.Windows.Navigation;
using System.Collections.ObjectModel;
using Prism.Windows.AppModel;
using Prism.Commands;
using System.Threading.Tasks;
using System.ComponentModel;

namespace CloudSeeder.ViewModels
{
    public class AddGitHubRepoPageViewModel : ViewModelBase
    {
        private GitHubController _controller;
        private GitHubControllerParameters _parameters;
        ApplicationDataContainer appSettings = null;
        public ObservableCollection<Repository> repos { get; set; }
        private readonly INavigationService _navigationService;
        public Repository SelectedValue { get; set; }
        public DelegateCommand MoveNextCommand { get; private set; }

        public AddGitHubRepoPageViewModel(INavigationService navigationService, IResourceLoader resourceLoader)
        {
            DisplayText = "GitHub Repo, Yeah Man!";
            MoveNextCommand = DelegateCommand.FromAsyncHandler(MoveNext, canMoveNext);
            appSettings = ApplicationData.Current.RoamingSettings;
            _navigationService = navigationService;

            _parameters = new GitHubControllerParameters
            {
                APIKey = appSettings.Values["gitHubApiToken"].ToString(),
                GitHubUser = appSettings.Values["gitHubUsername"].ToString(),
                RepositoryName = "testdeployments"
            };

            _controller = new GitHubController(_parameters);
            repos = new ObservableCollection<Repository>();
        }

        public async override void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            var user = await _controller.GetUserInfo();
            var repositories = await _controller.GetRepositoryList();

            foreach (var item in repositories)
                repos.Add(item);

            base.OnNavigatedTo(e, viewModelState);
        }

        public string DisplayText { get; set; }

        public virtual async Task MoveNext()
        {
            _navigationService.Navigate(PageTokens.AddTemplateFilePage, SelectedValue);
        }

        private bool canMoveNext()
        {
            return true;
        }
    }
}
