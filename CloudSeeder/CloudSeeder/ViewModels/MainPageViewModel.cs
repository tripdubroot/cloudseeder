﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using Windows.UI.Popups;
using deployToAzure.libs.Common;
using Windows.Storage;
using Windows.Security.Credentials;
using Windows.Security.Authentication.Web.Core;
using System.ComponentModel;
using Prism.Windows.AppModel;

namespace CloudSeeder.ViewModels
{
    public class MainPageViewModel : ViewModelBase, INotifyPropertyChanged
    {
        ApplicationDataContainer appSettings = null;
        WebAccountProvider wap = null;
        public string SignedInText { get; private set; }
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly INavigationService _navigationService;

        public MainPageViewModel(INavigationService navigationService, IResourceLoader resourceLoader)
        {
            _navigationService = navigationService;
            DisplayText = "0 Seeds Found. Select add from slider menu.";
        }

        public string DisplayText { get; set; }

        public async override void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            if (Globals.Token == null)
            {
                wap = await WebAuthenticationCoreManager.FindAccountProviderAsync("https://login.microsoft.com", Globals.authority);
                appSettings = ApplicationData.Current.RoamingSettings;
                WebTokenRequest wtr = new WebTokenRequest(wap, string.Empty, Globals.clientId);
                wtr.Properties.Add("resource", Globals.resource);
                // The WebAccount object is no longer available. Let's attempt a sign in with the saved username
                wtr.Properties.Add("LoginHint", appSettings.Values["login_hint"].ToString());
                WebTokenRequestResult wtrr = await WebAuthenticationCoreManager.RequestTokenAsync(wtr);
                if (wtrr.ResponseStatus == WebTokenRequestStatus.Success)
                {
                    Globals.userAccount = wtrr.ResponseData[0].WebAccount;
                    Globals.Token = wtrr.ResponseData[0].Token;

                    UpdateUXonSignIn();
                }
            }

            base.OnNavigatedTo(e, viewModelState);
        }

        private void UpdateUXonSignIn()
        {
            appSettings.Values["userID"] = Globals.userAccount.Id;
            appSettings.Values["login_hint"] = Globals.userAccount.UserName;
            SignedInText = string.Format("Welcome, {0}", Globals.userAccount.UserName);

            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(SignedInText)));
            }

        }
    }
}
