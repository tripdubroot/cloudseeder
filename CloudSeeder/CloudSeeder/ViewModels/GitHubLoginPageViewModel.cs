﻿using deployToAzure.libs.Controllers;
using deployToAzure.libs.Models;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Windows.AppModel;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace CloudSeeder.ViewModels
{
    public class GitHubLoginPageViewModel : ViewModelBase
    {
        ApplicationDataContainer appSettings = null;

        public GitHubLoginPageViewModel(INavigationService navigationService, IResourceLoader resourceLoader)
        {
            _navigationService = navigationService;
            LoginCommand = DelegateCommand.FromAsyncHandler(Login, canLogin);
            appSettings = ApplicationData.Current.RoamingSettings;
        }

        public string Username { get; set; }
        public string Password { get; set; }

        public DelegateCommand LoginCommand { get; private set; }
        private readonly INavigationService _navigationService;

        private async Task Login()
        {
            appSettings.Values["gitHubUsername"] = Username;

            string response = await GitHubController.GetGitAPIToken(Username, Password);
            var result = JsonConvert.DeserializeObject<GitHubAuthResponse>(response);

            if (result.token != "")
                appSettings.Values["gitHubApiToken"] = result.token;

            _navigationService.Navigate(PageTokens.MainPage, null);

            //TODO: Bad Password Path
        }

        private bool canLogin()
        {
            return true;
        }
    }
}
