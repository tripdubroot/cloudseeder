﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.ApplicationSettings;
using Prism.Windows.AppModel;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using Prism.Commands;

namespace CloudSeeder.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private bool _canNavigateToMain = false;
        private bool _canNavigateToAddManual = true;
        private bool _canNavigateToGitHub = true;
        private bool _canNavigateToSettings = true;

        public MenuViewModel(INavigationService navigationService, IResourceLoader resourceLoader)
        {
            _navigationService = navigationService;

            Commands = new ObservableCollection<MenuItemViewModel>
            {
                new MenuItemViewModel { DisplayName = resourceLoader.GetString("CloudSeedsMenuItemDisplayName"), FontIcon = "\ue15f", Command = new DelegateCommand(NavigateToMainPage, CanNavigateToMainPage)},
                new MenuItemViewModel { DisplayName = resourceLoader.GetString("AddGitHubRepoMenuItemDisplayName"), FontIcon = "\ue0a5", Command = new DelegateCommand(NavigateToGitHub, CanNavigateToGitHub)},
                new MenuItemViewModel { DisplayName = resourceLoader.GetString("AddManualPageMenuItemDisplayName"), FontIcon = "\ue19f", Command = new DelegateCommand(NavigateToAddManualPage, CanNavigateToAddManualPage)},
                new MenuItemViewModel { DisplayName = resourceLoader.GetString("SettingsMenuItemDisplayName"), FontIcon = "\ue713", Command = new DelegateCommand(NavigateToSettingsPage, CanNavigateToSettingsPage)}
            };
        }

        private void NavigateToSettingsPage()
        {
            if (CanNavigateToSettingsPage())
            {
                if (_navigationService.Navigate(PageTokens.SettingsPage, null))
                {
                    _canNavigateToMain = true;
                    _canNavigateToAddManual = true;
                    _canNavigateToGitHub = true;
                    _canNavigateToSettings = false;
                    RaiseCanExecuteChanged();
                }
            }
        }

        private void NavigateToAddManualPage()
        {
            if (CanNavigateToAddManualPage())
            {
                if (_navigationService.Navigate(PageTokens.AddManualPage, null))
                {
                    _canNavigateToMain = true;
                    _canNavigateToAddManual = false;
                    _canNavigateToGitHub = true;
                    _canNavigateToSettings = true;
                    RaiseCanExecuteChanged();
                }
            }
        }

        private void NavigateToGitHub()
        {
            if (CanNavigateToGitHub())
            {
                if (_navigationService.Navigate(PageTokens.AddGitHubRepoPage, null))
                {
                    _canNavigateToMain = true;
                    _canNavigateToAddManual = true;
                    _canNavigateToGitHub = false;
                    _canNavigateToSettings = true;
                    RaiseCanExecuteChanged();
                }
            }
        }

        private void NavigateToMainPage()
        {
            if (CanNavigateToMainPage())
            {
                if (_navigationService.Navigate(PageTokens.MainPage, null))
                {
                    _canNavigateToMain = false;
                    _canNavigateToAddManual = true;
                    _canNavigateToGitHub = true;
                    _canNavigateToSettings = true;
                    RaiseCanExecuteChanged();
                }
            }
        }

        private bool CanNavigateToSettingsPage()
        {
            return _canNavigateToSettings;
        }

        private bool CanNavigateToAddManualPage()
        {
            return _canNavigateToAddManual;
        }

        private bool CanNavigateToGitHub()
        {
            return _canNavigateToGitHub;
        }

        private bool CanNavigateToMainPage()
        {
            return _canNavigateToMain;
        }

        private void RaiseCanExecuteChanged()
        {
            foreach (var item in Commands)
            {
                var delegateCommand = item.Command as DelegateCommand;
                delegateCommand?.RaiseCanExecuteChanged();
            }
        }

        public ObservableCollection<MenuItemViewModel> Commands { get; set; } 
    }
}
