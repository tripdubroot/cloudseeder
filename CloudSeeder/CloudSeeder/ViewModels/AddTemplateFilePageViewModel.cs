﻿using Prism.Windows.AppModel;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Octokit;
using Prism.Commands;
using deployToAzure.libs.Controllers;
using Windows.Storage;
using Newtonsoft.Json;
using deployToAzure.libs.Models;
using System.Collections.ObjectModel;

namespace CloudSeeder.ViewModels
{
    public class AddTemplateFilePageViewModel : ViewModelBase
    {
        Octokit.Repository repository { get; set; }
        private readonly INavigationService _navigationService;
        public DelegateCommand MoveNextCommand { get; private set; }
        ApplicationDataContainer appSettings = null;
        public ObservableCollection<Item> files { get; set; }
        public Item SelectedValue { get; set; }

        public AddTemplateFilePageViewModel(INavigationService navigationService, IResourceLoader resourceLoader)
        {
            _navigationService = navigationService;
            appSettings = ApplicationData.Current.RoamingSettings;
            MoveNextCommand = DelegateCommand.FromAsyncHandler(MoveNext, canMoveNext);
            files = new ObservableCollection<Item>();
        }

        public async override void OnNavigatedTo(NavigatedToEventArgs e, Dictionary<string, object> viewModelState)
        {
            repository = e.Parameter as Octokit.Repository;

            GitHubControllerParameters parameters = new GitHubControllerParameters
            {
                GitHubUser = appSettings.Values["gitHubUsername"].ToString(),
                APIKey = appSettings.Values["gitHubApiToken"].ToString(),
                RepositoryName = repository.Name
            };

            WebRequestControllerParameters webParameters = new WebRequestControllerParameters
            {
                Url = parameters.JsonUris
            };

            WebRequestController webController = new WebRequestController(webParameters);

            var response = await webController.GetWebRequest();
            var result = JsonConvert.DeserializeObject<GitHubSearchItems>(response);

            foreach (var file in result.items)
                files.Add(file);

            base.OnNavigatedTo(e, viewModelState);
        }

        public virtual async Task MoveNext()
        {
            _navigationService.Navigate(PageTokens.AddParameterFilePage, repository);
        }

        private bool canMoveNext()
        {
            return true;
        }
    }
}
