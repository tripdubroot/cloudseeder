﻿using Prism.Commands;
using Prism.Windows.AppModel;
using Prism.Windows.Mvvm;
using Prism.Windows.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;
using deployToAzure.libs.Common;
using Windows.Storage;
using Windows.Security.Credentials;
using Windows.Security.Authentication.Web;
using Windows.Security.Authentication.Web.Core;
using System.ComponentModel;

namespace CloudSeeder.ViewModels
{
    public class AzureLoginPageViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private readonly INavigationService _navigationService;
        public DelegateCommand LoginCommand { get; private set; }

        public AzureLoginPageViewModel(INavigationService navigationService, IResourceLoader resourceLoader) : base()
        {
            _navigationService = navigationService;
            LoginCommand = DelegateCommand.FromAsyncHandler(Login, canLogin);
        }

        public virtual async Task Login()
        {
            _navigationService.Navigate(PageTokens.SubscriptionListPage, null);
        }

        private bool canLogin()
        {
            return true;
        }
    }
}
