﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.ComponentModel;
using CloudSeeder.ViewModels;

namespace CloudSeeder.Views
{
    public sealed partial class AddGitHubRepoPage : Page, INotifyPropertyChanged
    {
        public AddGitHubRepoPage()
        {
            this.InitializeComponent();
            this.DataContextChanged += AddGitHubRepoPage_DataContextChanged;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public AddGitHubRepoPageViewModel ConcreteDataContext => this.DataContext as AddGitHubRepoPageViewModel;

        private void AddGitHubRepoPage_DataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            var propertyChanged = PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(nameof(ConcreteDataContext)));
            }
        }
    }
}
