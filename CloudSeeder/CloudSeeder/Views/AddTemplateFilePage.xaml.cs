﻿using System.ComponentModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using CloudSeeder.ViewModels;

namespace CloudSeeder.Views
{
    public sealed partial class AddTemplateFilePage : Page, INotifyPropertyChanged
    {
        public AddTemplateFilePage()
        {
            this.InitializeComponent();
            this.DataContextChanged += AddTemplateFilePage_DataContextChanged;
        }

        private void AddTemplateFilePage_DataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            var propertyChanged = PropertyChanged;
            if (propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(nameof(ConcreteDataContext)));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public AddTemplateFilePageViewModel ConcreteDataContext => this.DataContext as AddTemplateFilePageViewModel;
    }
}
